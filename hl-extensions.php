<?php
/*
Plugin Name: HL Extensions
Description: Extensions for Healer's Library and Discover Healing
Author: Nick Temple
Version: 1.0
Author URI: http://nicktemple.com
*/


// Add an admin CSS file if this is a staging instance

if (getconst('HL_STAGING')) {

  function my_admin_theme_style() {
    wp_enqueue_style('my-admin-theme', plugins_url('/assets/wp-admin-staging.css', __FILE__));
  }
  add_action('admin_enqueue_scripts', 'my_admin_theme_style');
  add_action('login_enqueue_scripts', 'my_admin_theme_style');
}


// helpers
function getconst($const)
{
    return (defined($const)) ? constant($const) : null;
}


